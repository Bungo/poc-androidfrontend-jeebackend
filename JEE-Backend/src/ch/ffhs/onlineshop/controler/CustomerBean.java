package ch.ffhs.onlineshop.controler;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import ch.ffhs.onlineshop.model.Customer;

@Stateless
public class CustomerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager em;
	
	public Customer findByEmail(String email) {
		Customer c;
		try {
			c = (Customer) em.createQuery("FROM " + Customer.class.getSimpleName() + " c WHERE c.email = :email")
					.setParameter("email", email).getSingleResult();
			}catch(NoResultException e) {
				return null;
			}
		return c;
	}
	
	public Customer findById(Long id) {
		return em.find(Customer.class, id);
	}
	
	public Customer registration(String email, String password) { 
			
		Customer c = null;
		try {
		c = (Customer) em.createQuery("FROM " + Customer.class.getSimpleName() + " c WHERE c.email = :email")
				.setParameter("email", email).getSingleResult();
		}catch(NoResultException e) {
			c = new Customer();
			c.setEmail(email);
			c.setPassword(password);
			em.persist(c);
			em.flush();
			return c;
		}
			return null;
	}
}
