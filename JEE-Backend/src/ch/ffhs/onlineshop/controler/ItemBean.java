package ch.ffhs.onlineshop.controler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ch.ffhs.onlineshop.model.Customer;
import ch.ffhs.onlineshop.model.Item;

@Stateless
public class ItemBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager em;
	
	public List find(String searchKey) {
		if(searchKey.isEmpty()) {return new ArrayList();}
		return em.createQuery("FROM " + Item.class.getSimpleName() + " i WHERE i.title LIKE :key OR i.description LIKE :key")
				.setParameter("key", "%" + searchKey + "%")
				.getResultList();
	}
	
	public void putForSale(Item toSell) {
		em.persist(toSell);
		em.flush();
	}
	
	public void buy(Long item_id, Customer buyer) {
		Item i = em.find(Item.class, item_id);
		i.setBuyer(buyer);
		i.setSold(new Date(System.currentTimeMillis()));
		em.persist(i);
		em.flush();
	}
}
