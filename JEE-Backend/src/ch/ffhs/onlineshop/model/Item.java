package ch.ffhs.onlineshop.model;

import java.io.Serializable;

import javax.persistence.*;

import org.json.JSONException;
import org.json.JSONObject;

import sun.misc.BASE64Encoder;

import java.util.Date;


/**
 * The persistent class for the item database table.
 * 
 */
@Entity
@NamedQuery(name="Item.findAll", query="SELECT i FROM Item i")
public class Item implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String title;
	
	private String description;

	@Basic(fetch = FetchType.LAZY)
	@Lob
	private byte[] foto;

	private Double price;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="seller_id", referencedColumnName="id")
	})
	private Customer seller;

	@Temporal(TemporalType.TIMESTAMP)
	private Date sold;

	//bi-directional many-to-one association to Customer
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="buyer_id", referencedColumnName="id")
		})
	private Customer buyer;
	

	public Item() {
	}
	
	public Item(String description, byte[] foto, Double price, String title, Customer seller) {
		this.description = description;
		this.foto = foto;
		this.price = price;
		this.title = title;
		this.seller = seller;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getFoto() {
		return this.foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getSold() {
		return this.sold;
	}

	public void setSold(Date sold) {
		this.sold = sold;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Customer getSeller() {
		return this.seller;
	}

	public void setSeller(Customer seller) {
		this.seller = seller;
	}
	
	public Customer getBuyer() {
		return this.buyer;
	}
	
	public void setBuyer(Customer buyer) {
		this.buyer = buyer;
	}

	@Override
	public String toString() {
		BASE64Encoder enc = new BASE64Encoder();
		String base64Foto = enc.encodeBuffer(foto);
		try {
            JSONObject returnValue = new JSONObject().put("id", id)
            		.put("title", title)
            		.put("seller", new JSONObject().put("id", seller.getId()).put("email", seller.getEmail()))
            		.put("price", price)
            		.put("description", description)
            		.put("foto", base64Foto);
            if(buyer != null) {
            	returnValue.put("buyer", new JSONObject().put("id", buyer.getId()).put("email", buyer.getEmail()));
            	returnValue.put("sold", sold);
            }
            return returnValue.toString();
        } catch (JSONException e) {
            return null;
        }
	}
}