package ch.ffhs.onlineshop.api;




import java.io.IOException;

import javax.ejb.EJB;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ch.ffhs.onlineshop.controler.CustomerBean;
import ch.ffhs.onlineshop.controler.ItemBean;
import ch.ffhs.onlineshop.model.Customer;
import ch.ffhs.onlineshop.model.Item;
import sun.misc.BASE64Decoder;

@Path("/item")
public class ItemService {
	
	@EJB
	private ItemBean itemBean;
	
	@EJB
	private CustomerBean customerBean;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/search")
	public String search(@QueryParam("searchkey") String searchKey) {
		return itemBean.find(searchKey).toString();
	}
	
	@PUT
	@Path("/sell")
	public void sell(@FormParam("itemname") String title, @FormParam("description") String description,
			@FormParam("price") double price, @FormParam("foto") String foto, @FormParam("seller") Long seller_id) {
		Customer seller = customerBean.findById(seller_id);
		BASE64Decoder dec = new BASE64Decoder();
		byte[] decodedFoto;
		try {
			decodedFoto = dec.decodeBuffer(foto);
		} catch (IOException e) {
			e.printStackTrace();
			decodedFoto = null;
		}
		itemBean.putForSale(new Item(description, decodedFoto, price, title, seller));
	}
	
	@PUT
	@Path("/buy/{item}")
	public void buy(@PathParam("item") Long item_id, @FormParam("buyer") Long buyer_id) {
		Customer buyer = customerBean.findById(buyer_id);
		itemBean.buy(item_id, buyer);
	}

}
