package ch.ffhs.onlineshop.api;

import javax.ejb.EJB;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ch.ffhs.onlineshop.controler.CustomerBean;
import ch.ffhs.onlineshop.model.Customer;

@Path("/customer")
public class CustomerService {
	
	@EJB
	private CustomerBean customerBean;
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/login")
	public Long login(@FormParam("email") String email, @FormParam("password") String pw) {
		
		Customer c = customerBean.findByEmail(email);
		if(c == null || !c.getPassword().equals(pw)) {
			return (long) 0;
		}
		return c.getId();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/registration")
	public Long registration(@FormParam("email") String email, @FormParam("password") String pw) {
		Customer c = customerBean.registration(email, pw);
		if(c == null) {
			return (long) 0;
		}
		return c.getId();
	}
}
