package ch.ffhs.jamy.online_shop;

public class NavItem {
    String mTitle;
    String mSubtitle;
    int mIcon;

    public NavItem(String title, String subtitle, int icon){
        mTitle = title;
        mSubtitle = subtitle;
        mIcon = icon;
    }
}
