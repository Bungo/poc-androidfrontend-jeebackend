package ch.ffhs.jamy.online_shop;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class MainActivity extends AppCompatActivity {

    private String emailRegex = "\\w+@\\w+\\.\\w+";
    private String passwordRegex = ".+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Überprüft Radiobuttons und sendet entsprechend der Methode entweder die Registrierung oder das Login per API an den JEE Server.
     * In beiden Methoden wird der User bei Erfolg eingeloggt.
     * @param view
     */
    public void on_Send(View view) throws IOException {
        RadioButton rb = findViewById(R.id.radioButtonLogin);
        if(rb.isChecked()){
            login();
        }
        else{
            regist();
        }

    }

    /**
     * Leert alle Inputelemente
     * @param view
     */
    public void on_SetBack(View view){
        EditText toDelete = findViewById(R.id.email_login);
        toDelete.setText("");
        toDelete = findViewById(R.id.password_login);
        toDelete.setText("");
    }

    private void login() {
        Boolean validModel = true;
        EditText emailView = findViewById(R.id.email_login);
        String email = emailView.getText().toString();
        EditText passwordView = findViewById(R.id.password_login);
        String password = passwordView.getText().toString();
        if(!email.matches(emailRegex)){
            validModel = false;
            Toast.makeText(this, getString(R.string.email_exception),
                    Toast.LENGTH_SHORT).show();
        }
        if(!password.matches(passwordRegex)){
            validModel = false;
            Toast.makeText(this, getString(R.string.password_exception),
                    Toast.LENGTH_SHORT).show();
        }
        if(validModel){
            Long id = api_login(email, password);
            if(id > 0){
                Intent i = new Intent(this, HomeActivity.class);
                i.putExtra(Constants.USERNAME, email);
                i.putExtra(Constants.ID, id);
                startActivity(i);
                finish();
            }
            else{
                Toast.makeText(this, getString(R.string.login_exception),
                        Toast.LENGTH_SHORT).show();
            }

        }

    }

    private void regist() {
        Boolean validModel = true;
        EditText emailView = findViewById(R.id.email_login);
        String email = emailView.getText().toString();
        EditText passwordView = findViewById(R.id.password_login);
        String password = passwordView.getText().toString();
        if(!email.matches(emailRegex)){
            validModel = false;
            Toast.makeText(this, getString(R.string.email_exception),
                    Toast.LENGTH_SHORT).show();
        }
        if(!password.matches(passwordRegex)){
            validModel = false;
            Toast.makeText(this, getString(R.string.password_exception),
                    Toast.LENGTH_SHORT).show();
        }
        if(validModel) {
            Long id = api_regist(email, password);
            if(id > 0){
                Intent i = new Intent(this, HomeActivity.class);
                i.putExtra(Constants.USERNAME, email);
                i.putExtra(Constants.ID, id);
                startActivity(i);
                finish();
            }
            else{
                Toast.makeText(this, getString(R.string.connection_error),
                        Toast.LENGTH_SHORT).show();
            }
        }


    }

    private Long api_regist(final String email, final String password) {
        final String[] result = {""};
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                String urlParameters  = "email="+email.toLowerCase() + "&password="+password;
                byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
                int    postDataLength = postData.length;
                URL requestApi = null;
                try {
                    requestApi = new URL("http://10.0.2.2:8080/onlineshop-web/api/customer/registration/");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                HttpURLConnection conn =
                        null;
                try {
                    conn = (HttpURLConnection) requestApi.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                conn.setDoOutput( true );
                conn.setDoInput( true );
                conn.setInstanceFollowRedirects( false );
                try {
                    conn.setRequestMethod( "POST" );
                } catch (ProtocolException e1) {
                    e1.printStackTrace();
                }
                conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
                conn.setRequestProperty( "charset", "utf-8");
                conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
                conn.setUseCaches( false );
                try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
                    wr.write( postData );
                    wr.flush();
                    wr.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                try {
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String text;
                    while ((text = br.readLine()) != null) {
                        result[0] += text;
                    }

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                return "";
            }
        }.execute();
        int counter = 0;
        while (result[0] == "" && counter < 11){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
        }
        if(counter == 11){
            return Long.valueOf(0);
        }
        return Long.parseLong(result[0]);
    }

    private Long api_login(final String email, final String password) {
        final String[] result = {""};
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                String urlParameters  = "email="+email.toLowerCase() + "&password="+password;
                byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
                int    postDataLength = postData.length;
                URL requestApi = null;
                try {
                    requestApi = new URL("http://10.0.2.2:8080/onlineshop-web/api/customer/login/");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                HttpURLConnection conn =
                        null;
                try {
                    conn = (HttpURLConnection) requestApi.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                conn.setDoOutput( true );
                conn.setDoInput( true );
                conn.setInstanceFollowRedirects( false );
                try {
                    conn.setRequestMethod( "POST" );
                } catch (ProtocolException e1) {
                    e1.printStackTrace();
                }
                conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
                conn.setRequestProperty( "charset", "utf-8");
                conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
                conn.setUseCaches( false );
                try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
                    wr.write( postData );
                    wr.flush();
                    wr.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                try {
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String text;
                    while ((text = br.readLine()) != null) {
                        result[0] += text;
                    }

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                return "";
            }
        }.execute();
        int counter = 0;
        while (result[0] == "" && counter < 11){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
        }
        if(counter == 11){
            return Long.valueOf(0);
        }
        return Long.parseLong(result[0]);
    }
}
