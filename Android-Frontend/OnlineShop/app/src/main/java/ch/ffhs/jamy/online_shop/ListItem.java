package ch.ffhs.jamy.online_shop;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class ListItem extends LinearLayout{

    private ImageView fotoView;
    private TextView titleView;
    private TextView descriptionView;
    private LinearLayout itembox;
    private LinearLayout texthalf;
    private Button buybtn;


    public ListItem(Context context, int id, final Long buyerId, String title, String description, double price, byte[] foto) {
        super(context);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        lp.setMargins(30, 60, 0, 0);
        this.setLayoutParams(lp);
        setOrientation(LinearLayout.VERTICAL);

        titleView = new TextView(context);
        titleView.setText(title + " für " + price + " CHF");
        defaultLayoutParams(titleView);

        descriptionView = new TextView(context);
        descriptionView.setText(description);
        defaultLayoutParams(descriptionView);

        buybtn = new Button(context);
        buybtn.setText(R.string.buy);
        defaultLayoutParams(buybtn);
        final int obj_id = id;
        buybtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                api_buy(obj_id, buyerId);
            }
        });

        fotoView = new ImageView(context);
        Bitmap bitmap = BitmapFactory.decodeByteArray(foto, 0, foto.length);
        fotoView.setImageBitmap(bitmap);
        LayoutParams params = new LayoutParams(300, 300);
        fotoView.setLayoutParams(params);

        texthalf = new LinearLayout(context);
        lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        texthalf.setLayoutParams(lp);
        texthalf.setOrientation(LinearLayout.VERTICAL);
        texthalf.addView(titleView);
        texthalf.addView(descriptionView);

        itembox = new LinearLayout(context);
        lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        itembox.setLayoutParams(lp);
        itembox.setOrientation(LinearLayout.HORIZONTAL);
        itembox.addView(fotoView);
        itembox.addView(texthalf);

        addView(itembox);
        addView(buybtn);



    }

    private void defaultLayoutParams(View view){
        LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
    }

    private void api_buy(final int id, final Long buyerId){
        final int[] resultCode = new int[1];
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                String urlParameters  = "buyer="+ buyerId;
                byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
                int    postDataLength = postData.length;
                URL requestApi = null;
                try {
                    requestApi = new URL("http://10.0.2.2:8080/onlineshop-web/api/item/buy/"+ id);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                HttpURLConnection conn =
                        null;
                try {
                    conn = (HttpURLConnection) requestApi.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                conn.setDoOutput( true );
                conn.setInstanceFollowRedirects( false );
                try {
                    conn.setRequestMethod( "PUT" );
                } catch (ProtocolException e1) {
                    e1.printStackTrace();
                }
                conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
                conn.setRequestProperty( "charset", "utf-8");
                conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
                conn.setUseCaches( false );
                try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
                    wr.write( postData );
                    wr.flush();
                    wr.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                try {
                    resultCode[0] = conn.getResponseCode();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return "";
            }
        }.execute();
        int counter = 0;
        while (resultCode[0] == 0 && counter < 11){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
        }
        if(counter == 11 || resultCode[0] != 204){
            buybtn.setText(R.string.item_no_connection);
            buybtn.setEnabled(false);
        }
        else {
            buybtn.setText(R.string.sold);
            buybtn.setEnabled(false);
        }

    }

}
