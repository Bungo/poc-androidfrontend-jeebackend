package ch.ffhs.jamy.online_shop;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class SearchFragment extends Fragment {

    private View mFragmentView;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentView = inflater.inflate(R.layout.search_fragment, container, false);
        Button btn = mFragmentView.findViewById(R.id.search_send);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    on_Send(v);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        btn = mFragmentView.findViewById(R.id.search_setback);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                on_SetBack(v);
            }
        });
        return mFragmentView;
    }

    public void on_SetBack(View view){
        EditText searchtext = mFragmentView.findViewById(R.id.searchText);
        searchtext.setText("");
        LinearLayout itemlist = mFragmentView.findViewById(R.id.item_list);
        itemlist.removeAllViews();
    }

    public void on_Send(View view) throws IOException {
        EditText searchtext = mFragmentView.findViewById(R.id.searchText);
        try {
            search_api(searchtext.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void search_api(final String search) throws IOException, JSONException {
        URL loginAPI = new URL("http://localhost:8080/onlineshop-web/api/customer/search?searchkey=" + search);
        final String[] result = {""};
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                URL requestApi = null;
                try {
                    requestApi = new URL("http://10.0.2.2:8080/onlineshop-web/api/item/search?searchkey=" + search);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                HttpURLConnection conn =
                        null;
                try {
                    conn = (HttpURLConnection) requestApi.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                conn.setDoInput( true );
                conn.setInstanceFollowRedirects( false );
                try {
                    conn.setRequestMethod( "GET" );
                } catch (ProtocolException e1) {
                    e1.printStackTrace();
                }
               // conn.setRequestProperty( "charset", "utf-8");
                conn.setUseCaches( false );
                try {
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String text;
                    while ((text = br.readLine()) != null) {
                        result[0] += text;
                    }

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                return "";
            }
        }.execute();
        int counter = 0;
        while (result[0] == "" && counter < 11){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
        }
        LinearLayout itemlist = mFragmentView.findViewById(R.id.item_list);
        if(counter == 11){
            Toast.makeText(mFragmentView.getContext(), getString(R.string.connection_error),
                    Toast.LENGTH_SHORT).show();
        }
        else if(result[0].equals("[]")){
            itemlist.removeAllViews();
            Toast.makeText(mFragmentView.getContext(), getString(R.string.no_entry),
                    Toast.LENGTH_SHORT).show();
        }
        else {
            itemlist.removeAllViews();
            JSONArray json = null;
            json = new JSONArray(result[0]);
            Long sellerId = getActivity().getIntent().getExtras().getLong(Constants.ID);
            for (int i = 0; i < json.length(); i++) {
                JSONObject jObj = json.getJSONObject(i);
                if(!jObj.has("buyer" ) && jObj.getJSONObject("seller").getLong("id") != sellerId) {
                    int id = jObj.getInt("id");
                    String title = jObj.getString("title");
                    String description = jObj.getString("description");
                    double price = jObj.getDouble("price");
                    String base64Foto = jObj.getString("foto");
                    byte[] endFoto = Base64.decode(base64Foto, Base64.DEFAULT);
                    itemlist.addView(new ListItem(mFragmentView.getContext(), id, sellerId, title, description, price, endFoto));
                }
            }
            if(itemlist.getChildCount() == 0){
                Toast.makeText(mFragmentView.getContext(), getString(R.string.no_entry),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

}
