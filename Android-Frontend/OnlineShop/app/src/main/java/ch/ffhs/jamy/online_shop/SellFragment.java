package ch.ffhs.jamy.online_shop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;

public class SellFragment extends Fragment {

    private View mFragmentView;
    private String nameRegex = "\\w.+";
    private String regexPrice = "[1-9][0-9]*(\\.[0-9]*)?";
    private byte[] rawFoto;

    public SellFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentView = inflater.inflate(R.layout.sell_fragment, container, false);

        Button btn = mFragmentView.findViewById(R.id.sell_send);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSend(v);
            }
        });
        btn = mFragmentView.findViewById(R.id.sell_setback);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSetBack(v);
            }
        });
        ImageButton ibtn = mFragmentView.findViewById(R.id.imageButton_picture);
        ibtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPhotoClick(v);
            }
        });
        return mFragmentView;
    }

    public void onSetBack(View view){
        setBack();
    }

    private void setBack(){
        EditText articletexts = mFragmentView.findViewById(R.id.article_title);
        articletexts.setText("");
        articletexts = mFragmentView.findViewById(R.id.article_description);
        articletexts.setText("");
        articletexts = mFragmentView.findViewById(R.id.article_price);
        articletexts.setText("");

        ImageView picture = mFragmentView.findViewById(R.id.article_foto);
        picture.setImageBitmap(null);
        picture.setVisibility(ImageView.GONE);
        mFragmentView.findViewById(R.id.imageButton_picture).setVisibility(ImageView.VISIBLE);
    }

    public void onSend(View view){
        double price = 0;
        boolean validModel = true;
        EditText texts = mFragmentView.findViewById(R.id.article_title);
        String title = texts.getText().toString();

        texts = mFragmentView.findViewById(R.id.article_description);
        String description = texts.getText().toString();

        ImageView fotoHolder = mFragmentView.findViewById(R.id.article_foto);

        texts = mFragmentView.findViewById(R.id.article_price);
        if(texts.getText().toString().matches(regexPrice)){
            DecimalFormat df = new DecimalFormat("#.##");
           price = Double.parseDouble(texts.getText().toString());
           price = Double.parseDouble(df.format(price));
        }
        else{
            validModel = false;
            Toast.makeText(mFragmentView.getContext(), getString(R.string.productprice_exception),
                    Toast.LENGTH_SHORT).show();
        }


        if(!title.matches(nameRegex)){
            validModel = false;
            Toast.makeText(mFragmentView.getContext(), getString(R.string.productname_exception),
                    Toast.LENGTH_SHORT).show();
        }
        if(fotoHolder.getVisibility() != ImageView.VISIBLE){
            validModel = false;
            Toast.makeText(mFragmentView.getContext(), getString(R.string.photo_exception),
                    Toast.LENGTH_SHORT).show();
        }
        if(validModel){
            send_item_api(title, description, price);
        }



    }

    public void onPhotoClick(View view){
        selectImage(mFragmentView.getContext());
    }

    private void send_item_api(final String title, final String descripton, final double price) {
        final String readyFoto = Base64.encodeToString(rawFoto, Base64.DEFAULT);
        final Long sellerId = getActivity().getIntent().getExtras().getLong(Constants.ID);
        final int[] resultCode = new int[1];
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                String urlParameters  = "itemname="+ title + "&description="+ descripton +
                        "&price=" + price + "&foto=" + readyFoto + "&seller=" + sellerId;
                byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
                int    postDataLength = postData.length;
                URL requestApi = null;
                try {
                    requestApi = new URL("http://10.0.2.2:8080/onlineshop-web/api/item/sell/");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                HttpURLConnection conn =
                        null;
                try {
                    conn = (HttpURLConnection) requestApi.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                conn.setDoOutput( true );
                conn.setInstanceFollowRedirects( false );
                try {
                    conn.setRequestMethod( "PUT" );
                } catch (ProtocolException e1) {
                    e1.printStackTrace();
                }
                conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
                conn.setRequestProperty( "charset", "utf-8");
                conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
                conn.setUseCaches( false );
                try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
                    wr.write( postData );
                    wr.flush();
                    wr.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                try {
                    resultCode[0] = conn.getResponseCode();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return "";
            }
        }.execute();
        int counter = 0;
        while (resultCode[0] == 0 && counter < 11){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
        }
        if(counter == 11 || resultCode[0] != 204){
            Toast.makeText(mFragmentView.getContext(), getString(R.string.connection_error),
                    Toast.LENGTH_SHORT).show();
        }
        else {
            setBack();
            Toast.makeText(mFragmentView.getContext(), getString(R.string.upload_success),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void selectImage(Context context) {
        final CharSequence[] options = { getString(R.string.take_photo), getString(R.string.choose_gallery),getString(R.string.cancel) };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.picker));

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals(getString(R.string.take_photo))) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals(getString(R.string.choose_gallery))) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);

                } else if (options[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageView placholder = mFragmentView.findViewById(R.id.article_foto);
        if(resultCode != Activity.RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == Activity.RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        placholder.setImageBitmap(selectedImage);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        selectedImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        rawFoto = stream.toByteArray();
                        mFragmentView.findViewById(R.id.imageButton_picture).setVisibility(ImageView.GONE);
                        placholder.setVisibility(ImageView.VISIBLE);
                    }

                    break;
                case 1:
                    if (resultCode == Activity.RESULT_OK && data != null) {
                        Uri selectedImage =  data.getData();
                        try {
                            final InputStream i = mFragmentView.getContext().getContentResolver().openInputStream(selectedImage);
                            final Bitmap image = BitmapFactory.decodeStream(i);
                            placholder.setImageBitmap(image);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            image.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            rawFoto = stream.toByteArray();
                            mFragmentView.findViewById(R.id.imageButton_picture).setVisibility(ImageView.GONE);
                            placholder.setVisibility(ImageView.VISIBLE);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                    break;
            }
        }
    }

}
