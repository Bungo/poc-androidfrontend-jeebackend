# PoC-AndroidFrontend-JeeBackend

The following project was created as PoC for a distributed online shop. Thereby a JEE backend was created, which is integrated via REST-API to an Android frontend. Because the module "Java EE" from the bachelor program was taught in German, the documentation is also written in German. 

## Installation Instructions

1. Install a local Glassfish server.
2. Create the web application so that the link "http://localhost:8080/onlineshop-web/" serves as an access interface to the Jakarta EE web application. Projectname/src can then be replaced with the contents of the "JEE-Backend".
3. Install Android Studio, create a new project and replace the projecetname/app/src/main with the content of the "Android-Frontend". 
4. Install MySQL.
5. Perform scripts and settings as described in documentation.
6. Start local MySQL server and local Glassfish server.
7. Start Android application on emulator (Will not work on real Android hardware or not without code changes, because the Glassfish server only runs locally on the computer).